import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import 'hammerjs';

// Components
import { AppComponent } from './app.component';
import { PrivateContainerComponent } from './private-container/private-container.component';

// Vain-Modules
import { SharedModule } from 'vain-shared';
import { CoreModule, ENV_TYPE } from 'vain-core';

// Modules
import { ToastrModule } from 'ngx-toastr';
import { environment } from '@environment';
import { JwtModule } from '@auth0/angular-jwt';
import { StoreModule } from '@ngrx/store';
import { NgSelectModule } from '@ng-select/ng-select';

export function getAccessToken() {
  return localStorage.getItem('access_token');
}

import localeCL from '@angular/common/locales/es-CL';
import localeCLExtra from '@angular/common/locales/extra/es-CL';
import { registerLocaleData } from '@angular/common';

registerLocaleData(localeCL, localeCLExtra);

const routes: Routes = [
  {
    path: 'menu',
    component: PrivateContainerComponent,
    children: [
      {
        path: '',
        loadChildren: './features/gestion-produccion-lazy.module#GestionProduccionLazyModule'
      }
    ]
  }
];

@NgModule({
  declarations: [AppComponent, PrivateContainerComponent],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    NgSelectModule,
    CoreModule.forRoot({
      environment: ENV_TYPE.CUSTOM,
      customEnv: environment
    }),
    SharedModule.forRoot({
      environment: ENV_TYPE.CUSTOM,
      customEnv: environment
    }),
    ToastrModule.forRoot({
      timeOut: 2000,
      preventDuplicates: true
    }),
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    }), // , enableTracing: true
    JwtModule.forRoot({
      config: {
        tokenGetter: getAccessToken
      }
    }),
    StoreModule.forRoot({}),
    StoreDevtoolsModule.instrument({
      name: 'Gestion produccion',
      maxAge: 10
    }), // para debug
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
