import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GestionProduccionModule } from './gestion-produccion/gestion-produccion.module';
import { environment } from '../../environments/environment';
import { ENV_TYPE } from 'vain-core';


@NgModule({
  imports: [
    CommonModule,
    GestionProduccionModule.forChild({
      environment: ENV_TYPE.CUSTOM,
      customEnv: environment
    })
  ],
  declarations: []
})
export class GestionProduccionLazyModule {}
