import { Injectable } from '@angular/core';
import { GestionProduccionModuleConfig } from './gestion-produccion.module';
import { environment } from '../../../environments/environment';
import { environment as qaEnv } from '../../../environments/environment.qa';
import { environment as capEnv } from '../../../environments/environment.cap';
import { environment as erpEnv } from '../../../environments/environment.prod';
import { ENV_TYPE } from 'vain-core';


@Injectable()
export class Config {
  static env = environment;

  static getApiEnvUrl(): string {
    return `${Config.env.apiUrl}`;
  }

  static Configure(conf: GestionProduccionModuleConfig) {
    switch (conf.environment) {
      case ENV_TYPE.DEVELOPMENT:
        this.setEnv(environment);
        break;
      case ENV_TYPE.QA:
        this.setEnv(qaEnv);
        break;
      case ENV_TYPE.CERTIFICATION:
        this.setEnv(capEnv);
        break;
      case ENV_TYPE.PROD:
        this.setEnv(erpEnv);
        break;
      case ENV_TYPE.CUSTOM:
        this.setEnv(conf.customEnv);
        break;
      default:
        this.setEnv(environment);
    }
  }

  private static setEnv(selectedEnv) {
    Config.env = selectedEnv;
  }
}
