export interface FiltrosAdmCuadrillas {
  centroCostoId: number;
  estandarId: number;
  departamentosId: number;
  zonaComunId: number;
}

export interface FiltrosMantenedorCuadrillas {
  faseId: number;
  cuadrillasId: number[];
  nivelId: number;
  especialidadId: number;
  activa: boolean;
  estandarizada: boolean;
  niveles: any;
}
export interface TipoProyectoModel {
  tipoProyectoId: number;
  estandar: string;
  departamentos: string;
  zonaComun: string;
  totalManoDeObra: number;
}

export interface FaseModel {
  faseId: number;
  faseAnteriorId: number;
  faseSiguienteId: number;
  faseIdFront: number;
  nombre: string;
  grupos: GrupoCuadrillaModel[];
}

export interface GrupoCuadrillaModel {
  grupoId: number;
  grupoAnteriorId: number;
  grupoSiguienteId: number;
  grupoIdFront: number;
  nombre: string
  cuadrillas: CuadrillaModel[];
  cuadrillasStatic: CuadrillaModel[];
  cuadrillasBackup: CuadrillaModel[];
}

export interface CuadrillaModel {
  cuadrillaId: number;
  cuadrillaAnteriorId: number;
  cuadrillaSiguienteId: number;
  cuadrillaIdFront: number;
  obligatoria: boolean;
  nombre: string;
  activo: boolean;
  totales: TotalesCuadrillasModel[];
  niveles: NivelesCuadrillaModel[];
}

export interface TotalesCuadrillasModel {
  tipoProyectoId: number;
  niveles: TotalesNivelesModel[];
}

export interface TotalesNivelesModel {
  nivelId: number;
  total: number;
}

interface NivelesCuadrillaModel {
  matrizCuadrillaId: number;
  nivelId: number;
  nivel: string;
  especialidad: string;
  activo: boolean;
  cantidadTeorica: number;
  cantidades: CantidadNivelesCuadrillaModel[];
}

interface CantidadNivelesCuadrillaModel {
  tipoProyectoId: number;
  cantidad: number;
}

export interface ResponseObtenerCuadrillasBase {
  tiposProyecto: TipoProyectoModel[];
  fases: FaseModel[];
}

// mantendor cuadrillas

export interface FaseCuadrillaModel {
  faseId?: number;
  nombreFase: string;
  backPermiteEliminar?: boolean;
  abreviatura: string;
  orden: number;
  modificado?: boolean;
  activo?: boolean;
  cuadrillas?: CuadrillaFaseModel[];
}

export interface CuadrillaFaseModel {
  cuadrillaId: number;
  tipoCuadrillaId: number;
  nombreCuadrilla: number;
  sePuedeEliminar: boolean;
  orden: number;
  modificado: boolean;
  requiereTiposProyecto: boolean;
  estandarizada: boolean;
  activo: boolean;
  nivelEspecialidades?: NivelEspecialidadesCuadrillaFaseModel[];
}

interface NivelEspecialidadesCuadrillaFaseModel {
  nivel: NivelModel;
  especialidad: EspecialidadModel;
  cantidades: CantidadesModel[];
  cantidadTeorica: number;
  matrizCuadrillaId: number;
  modificado: boolean;
  sePuedeEliminar: boolean;
  activo: boolean;
}

interface NivelModel {
  nivelId: number;
  nombre: string;
}

interface EspecialidadModel {
  especialidadId: number;
  nombre: string;
}

interface CantidadesModel {
  cantidad: number;
  tipoProyectoId: number;
  activo: boolean;
}

export interface RequestEditarCrearCuadrillaFase {
  cuadrillas: FaseCuadrillaModel[];
}

interface CantidadesTeoricasModel {
  tipoProyectoId: number;
  cantidad: number;
}

export interface RequestEditarCantidadesTeoricasModel {
  matrizCuadrillaId: number;
  cantidades: CantidadesTeoricasModel[];
}
