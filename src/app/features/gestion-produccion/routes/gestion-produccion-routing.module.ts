import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from 'vain-shared';

// Components
import { MenuComponent } from '../components/menu/menu.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';

import { MainAdminCuadrillasComponent } from '../components/cuadrillas/admin-cuadrillas/main-admin-cuadrillas/main-admin-cuadrillas.component';
import { MainMantenedorCuadrillasComponent } from '../components/cuadrillas/mantenedor-cuadrillas/main-mantenedor-cuadrillas/main-mantenedor-cuadrillas.component';
import { MainTipoProyectoComponent } from '../components/tipo-proyecto/main-tipo-proyecto/main-tipo-proyecto.component';
import { FormularioCrearEditarCuadrillafaseComponent } from '../components/cuadrillas/mantenedor-cuadrillas/formulario-crear-editar-cuadrillafase/formulario-crear-editar-cuadrillafase.component';
import { RouterMantenedorCuadrillasComponent } from '../components/cuadrillas/mantenedor-cuadrillas/router-mantenedor-cuadrillas/router-mantenedor-cuadrillas.component';
import { MainAdminSeguridadComponent } from '../components/seguridad/admin-seguridad/main-admin-seguridad/main-admin-seguridad.component';

const ROUTES: Routes = [
  {
    path: 'gestion-produccion',
    component: MenuComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      {
        path: 'admincuadrillas',
        component: MainAdminCuadrillasComponent,
        // canActivate: [AuthGuard],
        // canLoad: [AuthGuard],
      },
      {
        path: 'mantenedorcuadrillas',
        component: RouterMantenedorCuadrillasComponent,
        // canActivate: [AuthGuard],
        // canLoad: [AuthGuard],
        children: [
          { path: '', component: MainMantenedorCuadrillasComponent },
          {
            path: 'mantenedorcuadrillafase',
            component: FormularioCrearEditarCuadrillafaseComponent,
          },
        ],
      },
      {
        path: 'tipoproyecto',
        component: MainTipoProyectoComponent,
        // canActivate: [AuthGuard],
        // canLoad: [AuthGuard],
      },

      {
        path: 'admsegprod',
        component: MainAdminSeguridadComponent,
        // canActivate: [AuthGuard],
        // canLoad: [AuthGuard],
      },

      { path: '**', pathMatch: 'full', redirectTo: 'dashboard' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule],
})
export class GestionProduccionRouting {}
