import { SeguridadActionsType, ActionsSeguridad } from './seguridad.actions';

export interface SeguridadState {
  evaluacionSemana: any;
  seguridadSemanaId: number;
}

export const initialSeguridadState: SeguridadState = {
  evaluacionSemana: null,
  seguridadSemanaId: 0,
};

export function seguridadReducer(
  state = initialSeguridadState,
  action: ActionsSeguridad
): SeguridadState {
  switch (action.type) {
    case SeguridadActionsType.EVALUAR_SEMANA_SEGURIDAD: {
      return {
        ...state,
        evaluacionSemana: action.payload
      };
    }
    case SeguridadActionsType.GUARDAR_SEMANA_SEGURIDADID: {
      return {
        ...state,
        seguridadSemanaId: action.payload,
      };
    }
    case SeguridadActionsType.REINICIAR_STORE_SEGURIDAD:
      return Object.assign({}, initialSeguridadState);
    default:
      return state;
  }
}
