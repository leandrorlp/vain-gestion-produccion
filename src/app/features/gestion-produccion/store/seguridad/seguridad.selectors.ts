import { createSelector } from '@ngrx/store';
import {
  selectGestionProduccionModuleState,
  GestionProduccionState,
} from '../gestion-produccion.state';
import { SeguridadState } from './seguridad.reducers';

export const getSeguridadState = createSelector(
  selectGestionProduccionModuleState,
  (state: GestionProduccionState) => state.seguridad
);

export const selEvaluacionSemanaSeguridad = createSelector(
  getSeguridadState,
  (state: SeguridadState) => state.evaluacionSemana
);

export const selSemanaSeguridadId = createSelector(
  getSeguridadState,
  (state: SeguridadState) => state.seguridadSemanaId
);
