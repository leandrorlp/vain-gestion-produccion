import { Action } from '@ngrx/store';

export enum SeguridadActionsType {
  REINICIAR_STORE_SEGURIDAD = '[SEGURIDAD] Reiniciar store seguridad',
  EVALUAR_SEMANA_SEGURIDAD = '[SEGURIDAD] Consultar semana seguridad',
  GUARDAR_SEMANA_SEGURIDADID = '[SEGURIDAD] Consultar semana seguridad',
}

export class ReiniciarStoreSeguridad implements Action {
  readonly type = SeguridadActionsType.REINICIAR_STORE_SEGURIDAD;
  constructor() {}
}

export class EvaluarSemanaSeguridad implements Action {
  readonly type = SeguridadActionsType.EVALUAR_SEMANA_SEGURIDAD;
  constructor(public payload: any) {}
}

export class GuardarSemanaSeguridadId implements Action {
  readonly type = SeguridadActionsType.GUARDAR_SEMANA_SEGURIDADID;
  constructor(public payload: any) {}
}

export type ActionsSeguridad =
  | ReiniciarStoreSeguridad
  | EvaluarSemanaSeguridad
  | GuardarSemanaSeguridadId;
