import { createFeatureSelector } from '@ngrx/store';

import * as fromCuadrilla from './cuadrillas/cuadrillas.reducers';
import * as fromSeguridad from './seguridad/seguridad.reducers';

export interface GestionProduccionState {
  cuadrilla: fromCuadrilla.CuadrillaState;
  seguridad: fromSeguridad.SeguridadState;
}

export const gestionProduccionReducers = {
  cuadrilla: fromCuadrilla.cuadrillaReducer,
  seguridad: fromSeguridad.seguridadReducer,
};

export const selectGestionProduccionModuleState = createFeatureSelector<
  GestionProduccionState
>('gestionProduccionFeatureModule');
