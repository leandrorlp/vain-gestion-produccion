import { Action } from '@ngrx/store';
import {
  FiltrosAdmCuadrillas,
  FiltrosMantenedorCuadrillas,
} from '../../models/cuadrillas/cuadrillas.model';

export enum CuadrillasActionsType {
  REINICIAR_STORE_CUADRILLA = '[CUADRILLAS] Reiniciar store cuadrilla',
  GUARDAR_FILTROS_ADMCUADRILLA = '[CUADRILLAS] Guardar filtros admin cuadrilla',
  GUARDAR_FILTROS_MANCUADRILLA = '[CUADRILLAS] Guardar filtros mantenedor cuadrilla',
  CREAR_CUADRILLA_FASE = '[CUADRILLAS] Crear cuadrilla fase',
  EDITAR_CUADRILLA_FASE = '[CUADRILLAS] Editar cuadrilla fase',
  TIPO_OBJETO_SELECCIONADO = '[CUADRILLAS] Tipo objeto seleccionado',
  CANCELAR_EDITAR_CREAR_CUADRILLA = '[CUADRILLAS] Cancelar editar crear cuadrilla',
}

export class GuardarFiltrosAdmCuadrilla implements Action {
  readonly type = CuadrillasActionsType.GUARDAR_FILTROS_ADMCUADRILLA;
  constructor(public payload: FiltrosAdmCuadrillas) {}
}

export class GuardarFiltrosManCuadrilla implements Action {
  readonly type = CuadrillasActionsType.GUARDAR_FILTROS_MANCUADRILLA;
  constructor(public payload: FiltrosMantenedorCuadrillas) {}
}

export class CrearCuadrillaFase implements Action {
  readonly type = CuadrillasActionsType.CREAR_CUADRILLA_FASE;
  constructor(public payload: boolean) {}
}

export class EditarCuadrillaFase implements Action {
  readonly type = CuadrillasActionsType.EDITAR_CUADRILLA_FASE;
  constructor(public payload: boolean) {}
}

export class TipoObjetoSeleccionado implements Action {
  readonly type = CuadrillasActionsType.TIPO_OBJETO_SELECCIONADO;
  constructor(public payload: string) {}
}

export class CancelarCrearEditarCuadrilla implements Action {
  readonly type = CuadrillasActionsType.CANCELAR_EDITAR_CREAR_CUADRILLA;
  constructor() {}
}

export class ReiniciarStoreCuadrilla implements Action {
  readonly type = CuadrillasActionsType.REINICIAR_STORE_CUADRILLA;
  constructor() {}
}

export type ActionsCuadrillas =
  | GuardarFiltrosAdmCuadrilla
  | GuardarFiltrosManCuadrilla
  | CrearCuadrillaFase
  | EditarCuadrillaFase
  | TipoObjetoSeleccionado
  | ReiniciarStoreCuadrilla
  | CancelarCrearEditarCuadrilla;
