import { ActionsCuadrillas, CuadrillasActionsType } from './cuadrillas.actions';
import {
  FiltrosAdmCuadrillas,
  FiltrosMantenedorCuadrillas,
} from '../../models/cuadrillas/cuadrillas.model';

export interface CuadrillaState {
  filtrosAdmCuadrilla?: FiltrosAdmCuadrillas;
  filtrosManCuadrillas?: FiltrosMantenedorCuadrillas;
  editarCuadrillaFase: boolean;
  crearCuadrillaFase: boolean;
  tipoObjetoSeleccionado: string;
}

export const initialCuadrillaState: CuadrillaState = {
  filtrosAdmCuadrilla: {} as FiltrosAdmCuadrillas,
  filtrosManCuadrillas: {} as FiltrosMantenedorCuadrillas,
  editarCuadrillaFase: false,
  crearCuadrillaFase: false,
  tipoObjetoSeleccionado: null,
};

export function cuadrillaReducer(
  state = initialCuadrillaState,
  action: ActionsCuadrillas
): CuadrillaState {
  switch (action.type) {
    case CuadrillasActionsType.GUARDAR_FILTROS_ADMCUADRILLA: {
      return {
        ...state,
        filtrosAdmCuadrilla: action.payload,
      };
    }
    case CuadrillasActionsType.GUARDAR_FILTROS_MANCUADRILLA: {
      return {
        ...state,
        filtrosManCuadrillas: action.payload,
      };
    }
    case CuadrillasActionsType.CREAR_CUADRILLA_FASE: {
      return {
        ...state,
        crearCuadrillaFase: action.payload,
        editarCuadrillaFase: false,
      };
    }
    case CuadrillasActionsType.EDITAR_CUADRILLA_FASE: {
      return {
        ...state,
        crearCuadrillaFase: false,
        editarCuadrillaFase: action.payload,
      };
    }
    case CuadrillasActionsType.TIPO_OBJETO_SELECCIONADO: {
      return {
        ...state,
        tipoObjetoSeleccionado: action.payload,
      };
    }
    case CuadrillasActionsType.TIPO_OBJETO_SELECCIONADO: {
      return {
        ...state,
        tipoObjetoSeleccionado: action.payload,
      };
    }
    case CuadrillasActionsType.CANCELAR_EDITAR_CREAR_CUADRILLA: {
      return {
        ...state,
        crearCuadrillaFase: false,
        editarCuadrillaFase: false,
        tipoObjetoSeleccionado: null,
      };
    }
    case CuadrillasActionsType.REINICIAR_STORE_CUADRILLA:
      return Object.assign({}, initialCuadrillaState);
    default:
      return state;
  }
}
