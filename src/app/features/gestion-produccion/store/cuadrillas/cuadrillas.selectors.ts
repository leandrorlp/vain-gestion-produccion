import { createSelector } from '@ngrx/store';
import { selectGestionProduccionModuleState, GestionProduccionState } from '../gestion-produccion.state';
import { CuadrillaState } from './cuadrillas.reducers';



export const getCuadrillaState = createSelector(
  selectGestionProduccionModuleState,
  (state: GestionProduccionState) => state.cuadrilla
);

export const selFiltrosAdmCuadrilla = createSelector(
  getCuadrillaState,
  (state: CuadrillaState) => state.filtrosAdmCuadrilla
);

export const selFiltrosManCuadrilla = createSelector(
  getCuadrillaState,
  (state: CuadrillaState) => state.filtrosManCuadrillas
);

export const selEditarCuadrillaFase = createSelector(
  getCuadrillaState,
  (state: CuadrillaState) => state.editarCuadrillaFase
);

export const selCrearCuadrillaFase = createSelector(
  getCuadrillaState,
  (state: CuadrillaState) => state.crearCuadrillaFase
);

export const selTipoObjetoSeleccionado = createSelector(
  getCuadrillaState,
  (state: CuadrillaState) => state.tipoObjetoSeleccionado
);


