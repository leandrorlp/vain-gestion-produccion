import { Component, OnInit, OnChanges } from '@angular/core';
import { NavigationService } from 'vain-core';
import { MenuService, NegocioGlobalsService } from 'vain-shared';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnChanges {
  public showModuleMenu = false;
  public expandedMenu = false;
  public showSubmenuElements = false;
  public moduleMenu: any;
  public step = -1;
  desa = false;

  constructor(
    public navigationService: NavigationService,
    public menuService: MenuService,
    public servicio: NegocioGlobalsService
  ) {}

  ngOnInit() {
    this.desa = window.location.href.includes('localhost:');
    this.navigationService.showModuleMenu.subscribe(res => {
      this.showModuleMenu = res;
    });

    this.menuService.sectionMenu.subscribe(menu => {
      this.moduleMenu = menu;
    });
  }

  ngOnChanges() {
    this.navigationService.showModuleMenu.subscribe(res => {
      this.showModuleMenu = res;
    });
  }

  toggleSidebar() {
    this.navigationService.toggleSidebarVisibility();
    this.showModuleMenu = this.navigationService.moduleSidebarVisible;
  }

  expandMenu() {
    this.expandedMenu = !this.expandedMenu;
    if (!this.expandedMenu) {
      this.step = -1;
    }
  }

  updateDivStyle() {
    this.showSubmenuElements = !this.showSubmenuElements;
  }

  goToErp(idMenu) {
    this.menuService.goToErp(idMenu);
  }

  setStep(index: number) {
    this.step = index;

    if (!this.expandedMenu) {
      this.expandedMenu = true;
    }
  }
}
