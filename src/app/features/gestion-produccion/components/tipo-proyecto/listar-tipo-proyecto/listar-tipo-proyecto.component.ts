import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { TipoProyectoService } from '../../../services/tipo-proyecto/tipo-proyecto.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import {from} from 'rxjs';
import { AgregarTipoComponent } from '../agregar-tipo/agregar-tipo.component';

@Component({
  selector: 'listar-tipo-proyecto',
  templateUrl: './listar-tipo-proyecto.component.html',
  styleUrls: ['./listar-tipo-proyecto.component.scss']
})
export class ListarTipoProyectoComponent implements OnInit {
  @Input() pageSizeOptions = [5, 10, 25, 100];
  @Input() pageSize = 10;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  estandars;
  estandarSelected;
  zonas;
  zonaSelected;
  dptos;
  cargando = true;
  dptosSelected;
  tieneDetalle = false;
  length;
  tabla;
  page;
  filtrado = false;
  displayedColumns: string[] = [
    'nombre',
    'estandar',
    'dpto',
    'zona',
    'acciones',
  ];

  constructor(
    public dialog: MatDialog,
    private tipos: TipoProyectoService
  ) { }

  ngOnInit() {
    this.tipos.listarEstandar().then(res => {
      this.estandars = res;
    });
    this.tipos.listarZonasComunes().then(res => {
      this.zonas = res;
    });
    this.tipos.listarDepartamentos().then(res => {
      this.dptos = res;
    });

    this.cargarData();
  }

  quitarFiltro() {
    this.filtrado = false;
    this.zonaSelected = null;
    this.dptosSelected = null;
    this.estandarSelected = null;

    this.cargarData();
  }

  getNext(evt) {
    this.page = evt.pageIndex + 1;
    this.pageSize = evt.pageSize;
    this.cargarData();
  }

  editar(element) {
    const dialog = this.dialog.open(AgregarTipoComponent, {
      panelClass: ['custom-dialog-container', 'custom-dialog-content'],
      data: { zonas: this.zonas, dptos: this.dptos, estandars: this.estandars, tipoSelected: element }
    });

    dialog.afterClosed().subscribe(result => {
      if (result) {
        this.tipos.crearEditar(result).then(res => {
          this.cargarData();
        });
      }
    });
  }

  crear() {
    const dialog = this.dialog.open(AgregarTipoComponent, {
      panelClass: ['custom-dialog-container', 'custom-dialog-content'],
      data: { zonas: this.zonas, dptos: this.dptos, estandars: this.estandars, tipoSelected: null }
    });

    dialog.afterClosed().subscribe(result => {
      if (result) {
        this.tipos.crearEditar(result).then(res => {
          this.cargarData();
        });
      }
    });
  }

  cargarData() {
    this.cargando = true;

    const request = {
      departamentoId: this.dptosSelected || null,
      zonaComunId: this.zonaSelected || null,
      estandarId: this.estandarSelected || null,
      page: this.page,
      pageSize: this.pageSize,
    };

    this.tipos.listarTiposProyecto(request).then((res: any) => {
      this.length = res.rowCount;
      this.tieneDetalle = this.length >0;
      this.cargando = false;
      this.tabla = new MatTableDataSource(res.results);
    });
  }

}
