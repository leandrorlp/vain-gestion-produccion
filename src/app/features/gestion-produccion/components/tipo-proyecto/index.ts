import { MainTipoProyectoComponent } from './main-tipo-proyecto/main-tipo-proyecto.component';
import { ListarTipoProyectoComponent } from './listar-tipo-proyecto/listar-tipo-proyecto.component';
import { AgregarTipoComponent } from './agregar-tipo/agregar-tipo.component';

export const CompTipoProyecto = [
  ListarTipoProyectoComponent,
  AgregarTipoComponent,
  MainTipoProyectoComponent
];

export const EntriesTipoProyecto = [
  AgregarTipoComponent
];
