import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-agregar-tipo',
  templateUrl: './agregar-tipo.component.html',
  styleUrls: ['./agregar-tipo.component.scss']
})
export class AgregarTipoComponent implements OnInit {
  estandars;
  estandarSelected;
  zonas;
  zonaSelected;
  dptos;
  dptosSelected;
  nombre;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AgregarTipoComponent>,) { }

  ngOnInit() {
    this.estandars = this.data.estandars;
    this.dptos = this.data.dptos;
    this.zonas = this.data.zonas;

    if(this.data.tipoSelected) {
      this.zonaSelected = this.zonas.find(x => x.id = this.data.tipoSelected.zonaComunId);
      this.dptosSelected = this.dptos.find(x => x.id = this.data.tipoSelected.departamentoId);
      this.estandarSelected = this.estandars.find(x => x.id = this.data.tipoSelected.estandarId);
      this.nombre = this.data.tipoSelected.nombre;
    }
  }

  agregarEstandar(name) {
    return { nombre: name, tag: true, id: 0 };
  }

  agregarZona(name) {
    return { nombre: name, tag: true, id: 0 };
  }

  agregarDpto(name) {
    return { nombre: name, tag: true, id: 0 };
  }

  guardar() {
    const req = {
      tipoProyectoId: this.data.tipoSelected? this.data.tipoSelected.tipoProyectoId : null,
      zonaComunId: this.zonaSelected.id,
      zonaComun: this.zonaSelected.nombre,
      estandarId: this.estandarSelected.id,
      estandar: this.estandarSelected.nombre,
      departamentoId: this.dptosSelected.id,
      departamento: this.dptosSelected.nombre,
      nombre: this.nombre
    };

    this.dialogRef.close(req);
  }

}
