import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CuadrillaModel, TotalesCuadrillasModel, TotalesNivelesModel } from '../../../models/cuadrillas/cuadrillas.model';

@Component({
  selector: 'app-crear-cuadrilla-modal',
  templateUrl: './crear-cuadrilla-modal.component.html',
  styleUrls: ['./crear-cuadrilla-modal.component.scss']
})
export class CrearCuadrillaModalComponent implements OnInit {
  nombre: string;
  abreviatura: string;
  antesDe = -2;
  antesDeAnterior: string;
  cuadrillas: any;
  fin = -2;
  nodoFin: any = null;
  nuevaCuadrillaId: number;
  cuadrillasId;

  constructor(
    public dialogRef: MatDialogRef<CrearCuadrillaModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    console.log(this.data);
    this.cuadrillas = this.data.grupoSelected.cuadrillas;

    if(this.data.cuadrillaSelected) {
      this.nombre = this.data.cuadrillaSelected.nombre;
      this.antesDe = this.data.cuadrillaSelected.faseAnteriorId;
    }

    if(this.cuadrillas.length) {
      this.nodoFin = this.cuadrillas[this.cuadrillas.length - 1];
    }

    const ids = this.cuadrillas.map(x => x.cuadrillaIdFront);
    this.nuevaCuadrillaId = ids.length ? Math.max.apply(null, ids) + 1 : 1;
    this.cuadrillasId = this.cuadrillas.map(x => {
      return { cuadrillaId: x.cuadrillaIdFront, nombre: x.nombre };
    });
    this.cuadrillasId.push({cuadrillaId: this.fin, nombre: 'Mover al fin'});
  }

  insertarAntesDe(antesDe: number, nuevaCuadrilla: CuadrillaModel) {
    const index = this.cuadrillas.findIndex(x => x.cuadrillaIdFront == antesDe);
    const item = this.cuadrillas[index];
    const itemAnterior = this.cuadrillas.find(x => x.cuadrillaSiguienteId = item.cuadrillaIdFront);

    if(itemAnterior) {
      itemAnterior.cuadrillaSiguienteId = nuevaCuadrilla.cuadrillaIdFront;
      nuevaCuadrilla.cuadrillaAnteriorId = itemAnterior.cuadrillaIdFront;
    }

    nuevaCuadrilla.cuadrillaSiguienteId = item.cuadrillaSiguienteId;
    item.cuadrillaSiguienteId = nuevaCuadrilla.cuadrillaIdFront;

    this.cuadrillas.splice(index, 0, nuevaCuadrilla);
 }

 insertarAlFinal(nuevaCuadrilla: CuadrillaModel) {
   this.cuadrillas.push(nuevaCuadrilla);
 }

 guardar() {
  if(!this.data.cuadrillaSelected) {
    const nuevaCuadrilla: CuadrillaModel = {
      cuadrillaId: null,
      cuadrillaIdFront: this.nuevaCuadrillaId,
      nombre: this.nombre,
      cuadrillaAnteriorId: null,
      cuadrillaSiguienteId: null,
      totales: this.crearTotalesVacios(),
      obligatoria: false,
      activo: true,
      niveles: []
    };

    if(this.antesDe == this.fin && this.nodoFin) {
      this.nodoFin.cuadrillaSiguienteId = nuevaCuadrilla.cuadrillaIdFront;
      nuevaCuadrilla.cuadrillaAnteriorId = this.nodoFin.cuadrillaIdFront;
    }

    if(this.antesDe == this.fin || (this.nodoFin && this.cuadrillas.length == 1 && this.antesDe == this.nodoFin.cuadrillaIdFront)) {
      this.insertarAlFinal(nuevaCuadrilla);
    } else {
      this.insertarAntesDe(this.antesDe, nuevaCuadrilla);
    }

    this.dialogRef.close(true);
  }
 }

  crearTotalesVacios(): TotalesCuadrillasModel[] {
    return this.data.tiposProyecto.map(x => {
      const niveles: TotalesCuadrillasModel = {
        tipoProyectoId: x.tipoProyectoId,
        niveles: this.data.niveles.map(y => {
          const nuevo: TotalesNivelesModel = {
            nivelId: y.id,
            total: 0
          };
          return nuevo;
        })
      };
      return niveles;
    });
  }

}
