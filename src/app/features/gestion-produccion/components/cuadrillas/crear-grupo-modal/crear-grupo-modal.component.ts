import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { GrupoCuadrillaModel } from '../../../models/cuadrillas/cuadrillas.model';

@Component({
  selector: 'app-crear-grupo-modal',
  templateUrl: './crear-grupo-modal.component.html',
  styleUrls: ['./crear-grupo-modal.component.scss']
})
export class CrearGrupoModalComponent implements OnInit {
  nombre: string;
  abreviatura: string;
  antesDe = -2;
  antesDeAnterior: string;
  grupos: any;
  fin = -2;
  nodoFin: any = null;
  nuevoGrupoId: number;
  gruposId;

  constructor(
    public dialogRef: MatDialogRef<CrearGrupoModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    this.grupos = this.data.faseSelected.grupos;

    if(this.data.grupoSelected) {
      this.nombre = this.data.grupoSelected.nombre;
      this.abreviatura = this.data.grupoSelected.abreviatura;
      this.antesDe = this.data.grupoSelected.faseAnteriorId;
    }

    if(this.grupos.length) {
      this.nodoFin = this.grupos[this.grupos.length - 1];
    }

    const ids = this.grupos.map(x => x.grupoIdFront);
    this.nuevoGrupoId = ids.length ? Math.max.apply(null, ids) + 1 : 1;
    this.gruposId = this.grupos.map(x => {
      return { grupoId: x.grupoIdFront, nombre: x.nombre };
    });
    this.gruposId.push({grupoId: this.fin, nombre: 'Mover al fin'});
  }

  insertarAntesDe(antesDe: number, nuevoGrupo: GrupoCuadrillaModel) {
    const index = this.grupos.findIndex(x => x.grupoIdFront == antesDe);
    const item = this.grupos[index];
    const itemAnterior = this.grupos.find(x => x.grupoSiguienteId = item.grupoIdFront);

    if(itemAnterior) {
      itemAnterior.grupoSiguienteId = nuevoGrupo.grupoIdFront;
      nuevoGrupo.grupoAnteriorId = itemAnterior.grupoIdFront;
    }

    nuevoGrupo.grupoSiguienteId = item.grupoSiguienteId;
    item.grupoSiguienteId = nuevoGrupo.grupoIdFront;

    this.grupos.splice(index, 0, nuevoGrupo);
 }

 insertarAlFinal(nuevoGrupo: GrupoCuadrillaModel) {
   this.grupos.push(nuevoGrupo);
 }

 guardar() {
  if(!this.data.grupoSelected) {
    const nuevoGrupo: GrupoCuadrillaModel = {
      grupoId: null,
      grupoIdFront: this.nuevoGrupoId,
      nombre: this.nombre,
      grupoAnteriorId: null,
      grupoSiguienteId: null,
      cuadrillas: [],
      cuadrillasBackup: [],
      cuadrillasStatic: []
    };

    if(this.antesDe == this.fin && this.nodoFin) {
      this.nodoFin.grupoSiguienteId = nuevoGrupo.grupoIdFront;
      nuevoGrupo.grupoAnteriorId = this.nodoFin.grupoIdFront;
    }

    if(this.antesDe == this.fin || (this.nodoFin && this.grupos.length == 1 && this.antesDe == this.nodoFin.grupoIdFront)) {
      this.insertarAlFinal(nuevoGrupo);
    } else {
      this.insertarAntesDe(this.antesDe, nuevoGrupo);
    }

    this.dialogRef.close(true);
  }
 }

}
