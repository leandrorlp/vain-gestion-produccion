import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FaseModel } from '../../../models/cuadrillas/cuadrillas.model';

@Component({
  selector: 'app-crear-fase-modal',
  templateUrl: './crear-fase-modal.component.html',
  styleUrls: ['./crear-fase-modal.component.scss']
})
export class CrearFaseModalComponent implements OnInit {
  nombre: string;
  abreviatura: string;
  antesDe = -2;
  antesDeAnterior: string;
  fases: any;
  fin = -2;
  nodoFin: any = null;
  nuevaFaseId: number;

  constructor(
    public dialogRef: MatDialogRef<CrearFaseModalComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    if(this.data.faseSelected) {
      this.nombre = this.data.faseSelected.nombre;
      this.abreviatura = this.data.faseSelected.abreviatura;
      this.antesDe = this.data.faseSelected.faseAnteriorId;
    }

    if(this.data.fases.length) {
      this.nodoFin = this.data.fases[this.data.fases.length - 1];
    }

    const ids = this.data.fases.map(x => x.faseIdFront);
    this.nuevaFaseId = ids.length ? Math.max.apply(null, ids) + 1 : 1;
    this.fases = this.data.fases.map(x => {
      return { faseId: x.faseIdFront, nombre: x.nombre };
    });
    this.fases.push({faseId: this.fin, nombre: 'Mover al fin'});
  }

  guardar() {
    if(!this.data.faseSelected) {
      const nuevaFase: FaseModel = {
        faseId: null,
        faseIdFront: this.nuevaFaseId,
        nombre: this.nombre,
        faseAnteriorId: null,
        faseSiguienteId: null,
        grupos: []
      };

      if(this.antesDe == this.fin && this.nodoFin) {
        this.nodoFin.faseSiguienteId = nuevaFase.faseIdFront;
        nuevaFase.faseAnteriorId = this.nodoFin.faseIdFront;
      }

      if(this.antesDe == this.fin || (this.nodoFin && this.data.fases.length == 1 && this.antesDe == this.nodoFin.faseIdFront)) {
        this.insertarAlFinal(nuevaFase);
      } else {
        this.insertarAntesDe(this.antesDe, nuevaFase);
      }
    }

    this.dialogRef.close(true);
  }

  insertarAntesDe(antesDe: number, nuevaFase: FaseModel) {
     const index = this.data.fases.findIndex(x => x.faseIdFront == antesDe);
     const item = this.data.fases[index];
     const itemAnterior = this.data.fases.find(x => x.faseSiguienteId = item.faseIdFront);

     if(itemAnterior) {
       itemAnterior.faseSiguienteId = nuevaFase.faseIdFront;
       nuevaFase.faseAnteriorId = itemAnterior.faseIdFront;
     }

     nuevaFase.faseSiguienteId = item.faseSiguienteId;
     item.faseSiguienteId = nuevaFase.faseIdFront;

     this.data.fases.splice(index, 0, nuevaFase);
  }

  insertarAlFinal(nuevaFase: FaseModel) {
    this.data.fases.push(nuevaFase);
  }

}
