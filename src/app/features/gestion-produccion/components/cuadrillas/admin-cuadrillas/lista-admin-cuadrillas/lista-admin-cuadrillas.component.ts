import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';

import {
  DestroySubscriptionService,
  ProgressSpinnerService,
} from 'vain-shared';

import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import { selFiltrosAdmCuadrilla } from '../../../../store/cuadrillas/cuadrillas.selectors';
import { FiltrosAdmCuadrillas } from '../../../../models/cuadrillas/cuadrillas.model';
import { CuadrillasService } from '../../../../services/cuadrillas/cuadrillas.service';

@Component({
  selector: 'app-lista-admin-cuadrillas',
  templateUrl: './lista-admin-cuadrillas.component.html',
  styleUrls: ['./lista-admin-cuadrillas.component.scss'],
})
export class ListaAdminCuadrillasComponent implements OnInit {
  datosFiltros: FiltrosAdmCuadrillas;
  displayedColumns: string[] = [
    'proyecto',
    'estandar',
    'departamentos',
    'tipozona',
    'estado',
    'acciones',
  ];
  dataSource: any;
  constructor(
    private store: Store<GestionProduccionState>,
    private destroySubscription$: DestroySubscriptionService,
    private cuadrillasService: CuadrillasService,
    private progressSpinnerService: ProgressSpinnerService
  ) {}

  ngOnInit(): void {
    this.store
      .pipe(select(selFiltrosAdmCuadrilla), takeUntil(this.destroySubscription$))
      .subscribe((datosFiltros) => {
        this.datosFiltros = datosFiltros;
        this.listarCuadrillasAdm();
      });
  }

  asignarCuadrillaProyecto() {}

  async listarCuadrillasAdm() {
    this.progressSpinnerService.openSpinner();
    this.dataSource = await this.cuadrillasService.listarCuadrillasAdm(
      this.datosFiltros
    );
    this.progressSpinnerService.closeSpinner();
  }
}
