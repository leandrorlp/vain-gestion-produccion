import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAdminCuadrillasComponent } from './lista-admin-cuadrillas.component';

describe('ListaAdminCuadrillasComponent', () => {
  let component: ListaAdminCuadrillasComponent;
  let fixture: ComponentFixture<ListaAdminCuadrillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaAdminCuadrillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAdminCuadrillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
