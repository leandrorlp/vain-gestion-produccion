import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainAdminCuadrillasComponent } from './main-admin-cuadrillas.component';

describe('MainAdminCuadrillasComponent', () => {
  let component: MainAdminCuadrillasComponent;
  let fixture: ComponentFixture<MainAdminCuadrillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainAdminCuadrillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainAdminCuadrillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
