import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';

import {
  NegocioGlobalsService,
  SelectorProyectoComponent,
  ProgressSpinnerService,
} from 'vain-shared';

import { CuadrillasService } from '../../../../services/cuadrillas/cuadrillas.service';
import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import { GuardarFiltrosAdmCuadrilla } from '../../../../store/cuadrillas/cuadrillas.actions';

@Component({
  selector: 'app-filtros-cuadrillas',
  templateUrl: './filtros-cuadrillas.component.html',
  styleUrls: ['./filtros-cuadrillas.component.scss'],
})
export class FiltrosCuadrillasComponent implements OnInit {
  @ViewChild('selectorProyecto', { static: true })
  selectorProyecto: SelectorProyectoComponent;
  projectGroupsSel = [];
  form: FormGroup;
  listaEstandar: any;
  listaDepartamentos: any;
  listaZonaComun: any;
  constructor(
    private negocioGlobalsService: NegocioGlobalsService,
    private progressSpinnerService: ProgressSpinnerService,
    private _formBuilder: FormBuilder,
    private cuadrillasService: CuadrillasService,
    private store: Store<GestionProduccionState>
  ) {}

  ngOnInit(): void {
    this.generarForm();
    this.cargarDatos();
  }

  async cargarDatos() {
    this.progressSpinnerService.openSpinner();
    this.selectorProyecto.projectGroups = await this.negocioGlobalsService.listarCentroCostoActivos();
    this.listaEstandar = await this.cuadrillasService.listarEstandar();
    this.listaDepartamentos = await this.cuadrillasService.listarDepartamentos();
    this.listaZonaComun = await this.cuadrillasService.listarZonaComun();
    this.progressSpinnerService.closeSpinner();
    this.filtrarLista();
  }

  generarForm() {
    this.form = this._formBuilder.group({
      estandarControl: ['', ''],
      departamentoControl: ['', ''],
      zonaComunControl: ['', ''],
    });
  }

  reiniciarFiltros() {}

  filtrarLista() {
    const request = {
      centroCostoId: this.selectorProyecto.proyectoControl.value || null,
      estandarId: this.form.get('estandarControl').value || null,
      departamentosId: this.form.get('departamentoControl').value || null,
      zonaComunId: this.form.get('zonaComunControl').value || null,
    };

    this.store.dispatch(new GuardarFiltrosAdmCuadrilla(request));
  }
}
