import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosCuadrillasComponent } from './filtros-cuadrillas.component';

describe('FiltrosCuadrillasComponent', () => {
  let component: FiltrosCuadrillasComponent;
  let fixture: ComponentFixture<FiltrosCuadrillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosCuadrillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosCuadrillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
