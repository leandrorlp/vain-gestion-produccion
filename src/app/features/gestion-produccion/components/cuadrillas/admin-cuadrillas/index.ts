import { MainAdminCuadrillasComponent } from './main-admin-cuadrillas/main-admin-cuadrillas.component';
import { FiltrosCuadrillasComponent } from './filtros-cuadrillas/filtros-cuadrillas.component';
import { ListaAdminCuadrillasComponent } from './lista-admin-cuadrillas/lista-admin-cuadrillas.component';

export const CompAdminCuadrillas = [
  MainAdminCuadrillasComponent,
  FiltrosCuadrillasComponent,
  ListaAdminCuadrillasComponent,
];
