import { Component, OnInit, Optional, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-crear-especialidad',
  templateUrl: './crear-especialidad.component.html',
  styleUrls: ['./crear-especialidad.component.scss']
})
export class CrearEspecialidadComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<CrearEspecialidadComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
  }

}
