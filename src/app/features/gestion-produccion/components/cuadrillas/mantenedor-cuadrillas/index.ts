import { RouterMantenedorCuadrillasComponent } from './router-mantenedor-cuadrillas/router-mantenedor-cuadrillas.component';
import { MainMantenedorCuadrillasComponent } from './main-mantenedor-cuadrillas/main-mantenedor-cuadrillas.component';
import { FiltrosMantenedorCuadrillasComponent } from './filtros-mantenedor-cuadrillas/filtros-mantenedor-cuadrillas.component';
import { ListaMantenedorCuadrillasComponent } from './lista-mantenedor-cuadrillas/lista-mantenedor-cuadrillas.component';
import { ListaCudrillasParticularesComponent } from './lista-cudrillas-particulares/lista-cudrillas-particulares.component';
import { ListaCuadrillasExcepcionalesComponent } from './lista-cuadrillas-excepcionales/lista-cuadrillas-excepcionales.component';
import { FormularioCrearEditarCuadrillafaseComponent } from './formulario-crear-editar-cuadrillafase/formulario-crear-editar-cuadrillafase.component';
import { FormularioFaseComponent } from './formulario-fase/formulario-fase.component';
import { FormularioCuadrillaComponent } from './formulario-cuadrilla/formulario-cuadrilla.component';
import { FormularioEspecialidadComponent } from './formulario-especialidad/formulario-especialidad.component';
import { CrearCuadrillaModalComponent } from '../crear-cuadrilla-modal/crear-cuadrilla-modal.component';
import { CrearEspecialidadComponent } from '../crear-especialidad/crear-especialidad.component';
import { CrearFaseModalComponent } from '../crear-fase-modal/crear-fase-modal.component';
import { CrearGrupoModalComponent } from '../crear-grupo-modal/crear-grupo-modal.component';


export const CompMantenedorCuadrillas = [
  RouterMantenedorCuadrillasComponent,
  MainMantenedorCuadrillasComponent,
  FiltrosMantenedorCuadrillasComponent,
  ListaMantenedorCuadrillasComponent,
  ListaCudrillasParticularesComponent,
  ListaCuadrillasExcepcionalesComponent,
  FormularioCrearEditarCuadrillafaseComponent,
  FormularioFaseComponent,
  FormularioCuadrillaComponent,
  FormularioEspecialidadComponent,
  CrearCuadrillaModalComponent,
  CrearEspecialidadComponent,
  CrearFaseModalComponent,
  CrearGrupoModalComponent
];

export const EntryCompMantenedorCuadrillas = [
  CrearCuadrillaModalComponent,
  CrearEspecialidadComponent,
  CrearFaseModalComponent,
  CrearGrupoModalComponent
];
