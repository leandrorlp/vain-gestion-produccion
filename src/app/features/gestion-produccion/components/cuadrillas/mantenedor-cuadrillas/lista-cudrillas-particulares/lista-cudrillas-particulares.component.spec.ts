import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCudrillasParticularesComponent } from './lista-cudrillas-particulares.component';

describe('ListaCudrillasParticularesComponent', () => {
  let component: ListaCudrillasParticularesComponent;
  let fixture: ComponentFixture<ListaCudrillasParticularesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCudrillasParticularesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCudrillasParticularesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
