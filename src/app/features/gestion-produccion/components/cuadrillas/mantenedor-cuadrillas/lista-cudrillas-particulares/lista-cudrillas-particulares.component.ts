import { takeUntil } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import {
  DestroySubscriptionService,
  ProgressSpinnerService,
  AlertsService
} from 'vain-shared';

import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import { selFiltrosManCuadrilla } from '../../../../store/cuadrillas/cuadrillas.selectors';
import {
  FiltrosMantenedorCuadrillas,
  CuadrillaModel,
} from '../../../../models/cuadrillas/cuadrillas.model';
import { CuadrillasService } from '../../../../services/cuadrillas/cuadrillas.service';

@Component({
  selector: 'app-lista-cudrillas-particulares',
  templateUrl: './lista-cudrillas-particulares.component.html',
  styleUrls: ['./lista-cudrillas-particulares.component.scss'],
})
export class ListaCudrillasParticularesComponent implements OnInit {
  datosFiltros: FiltrosMantenedorCuadrillas;
  data: CuadrillaModel[];
  hide = false;
  expandedRows = {};
  constructor(
    private store: Store<GestionProduccionState>,
    private destroySubscription$: DestroySubscriptionService,
    private cuadrillasService: CuadrillasService,
    private progressSpinnerService: ProgressSpinnerService,
    private alertsService: AlertsService
  ) {}

  ngOnInit(): void {
    this.store
      .pipe(
        select(selFiltrosManCuadrilla),
        takeUntil(this.destroySubscription$)
      )
      .subscribe((datosFiltros) => {
        this.datosFiltros = datosFiltros;
        this.obtenerCuadrillasParticulares();
      });
  }

  async obtenerCuadrillasParticulares() {
    this.progressSpinnerService.openSpinner();
    this.data = await this.cuadrillasService.obtenerCuadrillasParticulares(
      this.datosFiltros
    );
    await this.expandirFilas();
    this.progressSpinnerService.closeSpinner();
  }

  expandirContraer() {
    this.hide = !this.hide;
    if (this.hide === true) {
      this.expandedRows = {};
    } else {
      this.progressSpinnerService.openSpinner();
      this.expandirFilas().then((exp) => {
        this.progressSpinnerService.closeSpinner();
        this.expandedRows = exp;
      });
    }
  }

  expandirFilas(): Promise<any> {
    return new Promise((resolve) => {
      this.data.forEach((f) => {
        this.expandedRows[f.nombre] = true;
      });
      resolve(this.expandedRows);
    });
  }

  async activarDesactivarMatrizCuadrilla(matrizCuadrillaId) {
    this.progressSpinnerService.openSpinner();
    await this.cuadrillasService.activarDesactivarMatrizCuadrilla(
      matrizCuadrillaId
    );
    this.progressSpinnerService.closeSpinner();
    this.obtenerCuadrillasParticulares();
  }


  async activarDesactivarCuadrilla(cuadrillaId) {
    this.progressSpinnerService.openSpinner();
    await this.cuadrillasService.activarDesactivarCuadrilla(cuadrillaId);
    this.progressSpinnerService.closeSpinner();
    this.obtenerCuadrillasParticulares();
  }

  async eliminarCuadrilla(cuadrilla) {
    await this.alertsService
      .confirm(`Desea eliminar la cuadrilla: ${cuadrilla.nombre} ?`, false)
      .then((res) => {
        if (typeof res === 'object') {
          this.progressSpinnerService.openSpinner();
          this.cuadrillasService
            .eliminarCuadrilla(cuadrilla.cuadrillaId)
            .toPromise()
            .then(() => {
              this.progressSpinnerService.closeSpinner();
              this.obtenerCuadrillasParticulares();
            });
        }
      });
  }
}
