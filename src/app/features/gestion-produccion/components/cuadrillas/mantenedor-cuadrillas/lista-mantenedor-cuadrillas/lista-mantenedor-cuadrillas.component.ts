import { Store, select } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material';

import {
  DestroySubscriptionService,
  ProgressSpinnerService,
  AlertsService,
} from 'vain-shared';

import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import { CuadrillasService } from '../../../../services/cuadrillas/cuadrillas.service';
import { selFiltrosManCuadrilla } from '../../../../store/cuadrillas/cuadrillas.selectors';
import {
  FaseModel,
  TipoProyectoModel,
  ResponseObtenerCuadrillasBase,
  FiltrosMantenedorCuadrillas,
  RequestEditarCantidadesTeoricasModel,
} from '../../../../models/cuadrillas/cuadrillas.model';
import { CrearFaseModalComponent } from '../../crear-fase-modal/crear-fase-modal.component';
import {MenuItem} from 'primeng/api';
import { CrearGrupoModalComponent } from '../../crear-grupo-modal/crear-grupo-modal.component';
import { CrearCuadrillaModalComponent } from '../../crear-cuadrilla-modal/crear-cuadrilla-modal.component';

@Component({
  selector: 'app-lista-mantenedor-cuadrillas',
  templateUrl: './lista-mantenedor-cuadrillas.component.html',
  styleUrls: ['./lista-mantenedor-cuadrillas.component.scss'],
})
export class ListaMantenedorCuadrillasComponent implements OnInit {
  datosFiltros: FiltrosMantenedorCuadrillas;
  fases: FaseModel[];
  tiposProyecto: TipoProyectoModel[];
  responseObtenerCuadrillasBase: ResponseObtenerCuadrillasBase;
  expandedRows = {};
  hide = false;
  items: MenuItem[] = [];
  indiceSeleccionado = null;
  editing = false;
  constructor(
    private store: Store<GestionProduccionState>,
    private destroySubscription$: DestroySubscriptionService,
    private cuadrillasService: CuadrillasService,
    private progressSpinnerService: ProgressSpinnerService,
    private alertsService: AlertsService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.store
      .pipe(
        select(selFiltrosManCuadrilla),
        takeUntil(this.destroySubscription$)
      )
      .subscribe((datosFiltros) => {
        this.datosFiltros = datosFiltros;
        this.obtenerCuadrillasBase();
      });
  }

  async obtenerCuadrillasBase() {
    this.progressSpinnerService.openSpinner();
    this.responseObtenerCuadrillasBase = await this.cuadrillasService.obtenerCuadrillasBase(
      this.datosFiltros
    );
    this.fases = this.responseObtenerCuadrillasBase.fases;
    this.tiposProyecto = this.responseObtenerCuadrillasBase.tiposProyecto;
    await this.expandirFilas();
    this.progressSpinnerService.closeSpinner();
  }

  expandirContraer() {
    if (Object.entries(this.expandedRows).length) {
      this.expandedRows = {};
      this.fases.forEach(x => {
        console.log(x);
        x.grupos.forEach(y => {
          if(y.cuadrillas.length) {
            y.cuadrillasBackup = y.cuadrillas;
            y.cuadrillasStatic = y.cuadrillas;
            y.cuadrillas = [];
          }
        });
      });
    } else {
      this.expandirFilas().then((exp) => {
        this.expandedRows = exp;
      });
    }
  }

  mostrar(grupo) {
    if(grupo.cuadrillas.length) {
      grupo.cuadrillasBackup = grupo.cuadrillas;
      grupo.cuadrillasStatic = grupo.cuadrillas;
      grupo.cuadrillas = [];
    } else {
      grupo.cuadrillas = grupo.cuadrillasBackup;
      grupo.cuadrillasStatic = grupo.cuadrillasBackup;
      grupo.cuadrillasBackup = [];
    }
  }

  expandirFilas(): Promise<any> {
    return new Promise((resolve) => {
      this.fases.forEach((f) => {
        this.expandedRows[f.nombre] = true;

        f.grupos.forEach(y => {
          if(y.cuadrillasBackup && y.cuadrillasBackup.length) {
            y.cuadrillas = y.cuadrillasBackup;
            y.cuadrillasStatic = y.cuadrillas;
            y.cuadrillasBackup = [];
          }
        });
      });
      resolve(this.expandedRows);
    });
  }

  mostrarTotalesTipoProyecto(total) {
    let result = '';
    total.niveles.forEach((t) => {
      result += t.total;
    });

    return `(${result.split('').join('-')})`;
  }

  async estandarizarDesEstCuadrilla(cuadrillaId) {
    this.progressSpinnerService.openSpinner();
    await this.cuadrillasService.estandarizarDesEstCuadrilla(cuadrillaId);
    this.progressSpinnerService.closeSpinner();
    this.obtenerCuadrillasBase();
  }

  async activarDesactivarCuadrilla(cuadrillaId) {
    this.progressSpinnerService.openSpinner();
    await this.cuadrillasService.activarDesactivarCuadrilla(cuadrillaId);
    this.progressSpinnerService.closeSpinner();
    this.obtenerCuadrillasBase();
  }

  async eliminarCuadrilla(cuadrilla) {
    await this.alertsService
      .confirm(`Desea eliminar la cuadrilla: ${cuadrilla.nombre} ?`, false)
      .then((res) => {
        if (typeof res === 'object') {
          this.progressSpinnerService.openSpinner();
          this.cuadrillasService
            .eliminarCuadrilla(cuadrilla.cuadrillaId)
            .toPromise()
            .then(() => {
              this.progressSpinnerService.closeSpinner();
              this.obtenerCuadrillasBase();
            });
        }
      });
  }

  async activarDesactivarMatrizCuadrilla(matrizCuadrillaId) {
    this.progressSpinnerService.openSpinner();
    await this.cuadrillasService.activarDesactivarMatrizCuadrilla(
      matrizCuadrillaId
    );
    this.progressSpinnerService.closeSpinner();
    this.obtenerCuadrillasBase();
  }

  crearEditar(fase = null) {
    if(fase && !fase.nombreBackup) {
      fase.nombreBackup = fase.nombre;
      fase.abreviaturaBackup = fase.abreviatura;
      fase.antesDeBackup = fase.faseAnteriorId;
    }

    const dialog = this.dialog.open(CrearFaseModalComponent, {
      panelClass: [
        'custom-dialog-container-no-padding',
        'custom-dialog-content'
      ],
      data: { fases: this.fases, faseSelected: fase }
    });

    dialog
      .afterClosed()
      .pipe(takeUntil(this.destroySubscription$))
      .subscribe(res => {
        if(res) {
          this.fases = [...this.fases];
          console.log(this.fases);
        }
      });
  }

  crearEditarGrupo(fase, grupo = null) {
    if(grupo && !grupo.nombreBackup) {
      grupo.nombreBackup = grupo.nombre;
      grupo.abreviaturaBackup = grupo.abreviatura;
      grupo.antesDeBackup = grupo.grupoAnteriorId;
    }

    const dialog = this.dialog.open(CrearGrupoModalComponent, {
      panelClass: [
        'custom-dialog-container-no-padding',
        'custom-dialog-content'
      ],
      data: { faseSelected: fase, grupoSelected : grupo }
    });

    dialog
      .afterClosed()
      .pipe(takeUntil(this.destroySubscription$))
      .subscribe(res => {
        if (res) {
          this.fases = [...this.fases];
          console.log(fase.grupos);
        }
      });
  }

  crearEditarCuadrilla(grupo, cuadrilla = null) {
    if(grupo && !grupo.nombreBackup) {
      grupo.nombreBackup = grupo.nombre;
      grupo.abreviaturaBackup = grupo.abreviatura;
      grupo.antesDeBackup = grupo.grupoAnteriorId;
    }

    if(grupo.cuadrillasBackup.length) {
      grupo.cuadrillas = grupo.cuadrillasBackup;
      grupo.cuadrillasStatic = grupo.cuadrillasBackup;
      grupo.cuadrillasBackup = [];
    }

    const dialog = this.dialog.open(CrearCuadrillaModalComponent, {
      panelClass: [
        'custom-dialog-container-no-padding',
        'custom-dialog-content'
      ],
      data: { grupoSelected : grupo, cuadrillaSelected: cuadrilla, niveles: this.datosFiltros.niveles, tiposProyecto: this.tiposProyecto }
    });

    dialog
      .afterClosed()
      .pipe(takeUntil(this.destroySubscription$))
      .subscribe(res => {
        if (res) {
          this.fases = [...this.fases];
        }
      });
  }

  async guardarCantidadesTeoricas(row) {
    this.progressSpinnerService.openSpinner();
    const request: RequestEditarCantidadesTeoricasModel = {
      matrizCuadrillaId: row.matrizCuadrillaId,
      cantidades: row.cantidades,
    };

    await this.cuadrillasService.editarCantidadesTeoricas(request);
    this.indiceSeleccionado = null;
    this.progressSpinnerService.closeSpinner();
    this.obtenerCuadrillasBase();
  }

  editarFila(nivel) {
    this.indiceSeleccionado = nivel.matrizCuadrillaId;
  }
}
