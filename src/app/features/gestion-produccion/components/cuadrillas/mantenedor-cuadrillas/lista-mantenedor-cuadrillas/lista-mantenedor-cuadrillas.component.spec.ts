import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaMantenedorCuadrillasComponent } from './lista-mantenedor-cuadrillas.component';

describe('ListaMantenedorCuadrillasComponent', () => {
  let component: ListaMantenedorCuadrillasComponent;
  let fixture: ComponentFixture<ListaMantenedorCuadrillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMantenedorCuadrillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaMantenedorCuadrillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
