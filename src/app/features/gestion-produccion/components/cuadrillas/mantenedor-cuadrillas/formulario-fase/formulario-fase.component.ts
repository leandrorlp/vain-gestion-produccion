import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import {
  ProgressSpinnerService,
  DestroySubscriptionService,
} from 'vain-shared';

import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import { CuadrillasService } from '../../../../services/cuadrillas/cuadrillas.service';
import { selTipoObjetoSeleccionado } from '../../../../store/cuadrillas/cuadrillas.selectors';

@Component({
  selector: 'app-formulario-fase',
  templateUrl: './formulario-fase.component.html',
  styleUrls: ['./formulario-fase.component.scss'],
})
export class FormularioFaseComponent implements OnInit {
  form: FormGroup;
  listaFases: any;
  listaIndices = [];
  constructor(
    private _formBuilder: FormBuilder,
    private destroySubscription$: DestroySubscriptionService,
    private store: Store<GestionProduccionState>,
    private cuadrillasService: CuadrillasService,
    private progressSpinnerService: ProgressSpinnerService,
    private toastrService: ToastrService
  ) {}

  ngOnInit(): void {
    this.generarForm();
    this.store
      .pipe(
        select(selTipoObjetoSeleccionado),
        takeUntil(this.destroySubscription$)
      )
      .subscribe((tipoObjetoSeleccionado) => {
        if (tipoObjetoSeleccionado && tipoObjetoSeleccionado === 'fase') {
          this.cargarDatosFase();
        }
      });
  }

  generarForm() {
    this.form = this._formBuilder.group({
      nombreFaseControl: ['', Validators.required],
      abreviaturaControl: ['', Validators.required],
      antesDeControl: ['', Validators.required],
    });
  }

  agregarFaseAlista(evt) {
    evt.preventDefault();
    evt.stopPropagation();

    const currentData = JSON.parse(JSON.stringify(this.listaFases));


    const nuevaFase = {
      orden: this.form.get('antesDeControl').value,
      nombreFase: this.form.get('nombreFaseControl').value,
      abreviatura: this.form.get('abreviaturaControl').value,
      activo: null,
    };

    const existe = currentData.find(
      (fase) => fase.nombreFase === nuevaFase.nombreFase
    );

    if (!existe) {
      this.listaFases = null;
      currentData.forEach((fase) => {
        if (fase.orden >= nuevaFase.orden) {
          fase.orden = fase.orden + 1;
        }
      });

      currentData.splice(nuevaFase.orden - 1, 0, nuevaFase);

      this.listaFases = currentData;
      this.listaIndices = this.listaFases.map((val) => val.orden);
      this.form.reset();
    } else {
      this.toastrService.warning(`La fase ${existe.nombreFase} ya esta en la lista de fases.`);
    }
  }

  async cargarDatosFase() {
    this.progressSpinnerService.openSpinner();
    this.listaFases = await this.cuadrillasService.listarFases();
    this.listaIndices = this.listaFases.map((val) => val.orden);
    this.progressSpinnerService.closeSpinner();
  }
}
