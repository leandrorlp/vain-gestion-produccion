import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainMantenedorCuadrillasComponent } from './main-mantenedor-cuadrillas.component';

describe('MainMantenedorCuadrillasComponent', () => {
  let component: MainMantenedorCuadrillasComponent;
  let fixture: ComponentFixture<MainMantenedorCuadrillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainMantenedorCuadrillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainMantenedorCuadrillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
