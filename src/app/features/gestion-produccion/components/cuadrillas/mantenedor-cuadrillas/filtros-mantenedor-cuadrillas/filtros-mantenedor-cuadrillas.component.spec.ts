import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltrosMantenedorCuadrillasComponent } from './filtros-mantenedor-cuadrillas.component';

describe('FiltrosMantenedorCuadrillasComponent', () => {
  let component: FiltrosMantenedorCuadrillasComponent;
  let fixture: ComponentFixture<FiltrosMantenedorCuadrillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltrosMantenedorCuadrillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltrosMantenedorCuadrillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
