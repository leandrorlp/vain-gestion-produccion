import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';

import { ProgressSpinnerService } from 'vain-shared';

import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import { FiltrosMantenedorCuadrillas } from '../../../../models/cuadrillas/cuadrillas.model';
import { GuardarFiltrosManCuadrilla } from '../../../../store/cuadrillas/cuadrillas.actions';
import { CuadrillasService } from '../../../../services/cuadrillas/cuadrillas.service';

@Component({
  selector: 'app-filtros-mantenedor-cuadrillas',
  templateUrl: './filtros-mantenedor-cuadrillas.component.html',
  styleUrls: ['./filtros-mantenedor-cuadrillas.component.scss'],
})
export class FiltrosMantenedorCuadrillasComponent implements OnInit {
  form: FormGroup;
  listaFases: any;
  listaNivel: any;
  listaCuadrilla: any;
  activas= [{
    value: true,
    name: 'Activa'
  },
  {
    value: false,
    name: 'Inactiva'
  }];
  obligatorias = [{
    value: true,
    name: 'Obligatorias'
  },
  {
    value: false,
    name: 'No obligatorias'
  },{
    value: null,
    name: 'Todas'
  },];
  listaEspecialidad: any;
  constructor(
    private _formBuilder: FormBuilder,
    private cuadrillasService: CuadrillasService,
    private progressSpinnerService: ProgressSpinnerService,
    private store: Store<GestionProduccionState>
  ) {}

  ngOnInit(): void {
    this.generarForm();
    this.cargarDatos();
  }

  generarForm() {
    this.form = this._formBuilder.group({
      faseControl: [null, ''],
      nivelControl: [null, ''],
      cuadrillasControl: [[], ''],
      especialidadControl: [null, ''],
      estadoControl: [true, ''],
      estandarControl: [null, ''],
    });
  }

  async cargarDatos() {
    this.progressSpinnerService.openSpinner();
    this.listaFases = await this.cuadrillasService.listarFases();
    this.listaNivel = await this.cuadrillasService.listarNiveles();
    this.listaCuadrilla = await this.cuadrillasService.listarCuadrillas();
    this.listaEspecialidad = await this.cuadrillasService.listarEspecialidades();
    this.progressSpinnerService.closeSpinner();

    this.filtrarListas();
  }

  filtrarListas() {
    const request: FiltrosMantenedorCuadrillas = {
      faseId: this.form.get('faseControl').value || null,
      cuadrillasId: this.form.get('cuadrillasControl').value || [],
      nivelId: this.form.get('nivelControl').value || null,
      especialidadId:
        this.form.get('especialidadControl').value,
      activa: this.form.get('estadoControl').value,
      estandarizada: this.form.get('estandarControl').value,
      niveles: this.listaNivel
    };

    this.store.dispatch(new GuardarFiltrosManCuadrilla(request));
  }

  limpiarFiltros() {
    this.form.reset();
    this.store.dispatch(
      new GuardarFiltrosManCuadrilla({} as FiltrosMantenedorCuadrillas)
    );
  }
}
