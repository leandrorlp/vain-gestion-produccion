import { takeUntil } from 'rxjs/operators';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatStepper } from '@angular/material';
import { Store, select } from '@ngrx/store';
import {
  DestroySubscriptionService,
  ProgressSpinnerService,
} from 'vain-shared';

import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import {
  selTipoObjetoSeleccionado,
  selEditarCuadrillaFase,
  selCrearCuadrillaFase,
} from '../../../../store/cuadrillas/cuadrillas.selectors';
import { CancelarCrearEditarCuadrilla } from '../../../../store/cuadrillas/cuadrillas.actions';
import { CuadrillasService } from '../../../../services/cuadrillas/cuadrillas.service';
import { RequestEditarCrearCuadrillaFase } from '../../../../models/cuadrillas/cuadrillas.model';

@Component({
  selector: 'app-formulario-crear-editar-cuadrillafase',
  templateUrl: './formulario-crear-editar-cuadrillafase.component.html',
  styleUrls: ['./formulario-crear-editar-cuadrillafase.component.scss'],
})
export class FormularioCrearEditarCuadrillafaseComponent
  implements OnInit, OnDestroy {
  @ViewChild('stepper', { static: true }) stepper: MatStepper;
  modoCrear = false;
  modoEditar = false;
  request = {} as RequestEditarCrearCuadrillaFase;

  deshabilitarAceptarFase = false;
  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    private destroySubscription$: DestroySubscriptionService,
    private store: Store<GestionProduccionState>,
    private cuadrillasService: CuadrillasService,
    private progressSpinnerService: ProgressSpinnerService
  ) {}

  ngOnDestroy(): void {
    this.store.dispatch(new CancelarCrearEditarCuadrilla());
  }

  ngOnInit(): void {
    this.ngrxStoreInit();
  }

  ngrxStoreInit() {
    this.store
      .pipe(
        select(selTipoObjetoSeleccionado),
        takeUntil(this.destroySubscription$)
      )
      .subscribe((tipoObjetoSeleccionado) => {
        switch (tipoObjetoSeleccionado) {
          case 'cuadrilla':
            this.stepper.selectedIndex = 1;
            break;
          case 'fase':
            this.stepper.selectedIndex = 0;
            this.deshabilitarAceptarFase = true;
            break;
          default:
          //  this.volver();
        }
      });

    this.store
      .pipe(
        select(selEditarCuadrillaFase),
        takeUntil(this.destroySubscription$)
      )
      .subscribe((modoEditar) => (this.modoEditar = modoEditar));

    this.store
      .pipe(select(selCrearCuadrillaFase), takeUntil(this.destroySubscription$))
      .subscribe((modoCrear) => (this.modoCrear = modoCrear));
  }

  cambioSeleccion() {
    switch (this.stepper.selectedIndex) {
      case 0:
        // this.cargarDatosFase();
        break;
      case 1:
        // this.cargarDatosCuadrilla();
        break;
      case 2:
        break;
    }
  }

  reglasFormulario() {}

  volver() {
    const url = this.router.url;
    const arrayUrl = url.split('/');
    arrayUrl.shift();
    arrayUrl.pop();
    const newURL = arrayUrl.join('/');

    this.router.navigate([newURL]);
  }
}
