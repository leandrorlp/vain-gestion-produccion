import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioCrearEditarCuadrillafaseComponent } from './formulario-crear-editar-cuadrillafase.component';

describe('FormularioCrearEditarCuadrillafaseComponent', () => {
  let component: FormularioCrearEditarCuadrillafaseComponent;
  let fixture: ComponentFixture<FormularioCrearEditarCuadrillafaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioCrearEditarCuadrillafaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioCrearEditarCuadrillafaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
