import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioEspecialidadComponent } from './formulario-especialidad.component';

describe('FormularioEspecialidadComponent', () => {
  let component: FormularioEspecialidadComponent;
  let fixture: ComponentFixture<FormularioEspecialidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioEspecialidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioEspecialidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
