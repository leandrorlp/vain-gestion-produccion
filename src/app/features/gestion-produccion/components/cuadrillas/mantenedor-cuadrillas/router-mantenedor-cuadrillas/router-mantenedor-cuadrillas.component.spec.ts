import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterMantenedorCuadrillasComponent } from './router-mantenedor-cuadrillas.component';

describe('RouterMantenedorCuadrillasComponent', () => {
  let component: RouterMantenedorCuadrillasComponent;
  let fixture: ComponentFixture<RouterMantenedorCuadrillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RouterMantenedorCuadrillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterMantenedorCuadrillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
