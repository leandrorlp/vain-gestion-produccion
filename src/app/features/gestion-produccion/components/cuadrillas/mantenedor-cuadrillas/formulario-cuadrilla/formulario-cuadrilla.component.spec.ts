import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioCuadrillaComponent } from './formulario-cuadrilla.component';

describe('FormularioCuadrillaComponent', () => {
  let component: FormularioCuadrillaComponent;
  let fixture: ComponentFixture<FormularioCuadrillaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioCuadrillaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioCuadrillaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
