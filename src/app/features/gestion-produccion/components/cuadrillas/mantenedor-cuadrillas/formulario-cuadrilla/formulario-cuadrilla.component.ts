import { ToastrService } from 'ngx-toastr';
import {
  DestroySubscriptionService,
  ProgressSpinnerService,
} from 'vain-shared';
import { Store, select } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import { CuadrillasService } from '../../../../services/cuadrillas/cuadrillas.service';
import { selTipoObjetoSeleccionado } from '../../../../store/cuadrillas/cuadrillas.selectors';

@Component({
  selector: 'app-formulario-cuadrilla',
  templateUrl: './formulario-cuadrilla.component.html',
  styleUrls: ['./formulario-cuadrilla.component.scss'],
})
export class FormularioCuadrillaComponent implements OnInit {
  form: FormGroup;
  listaFases: any;
  listaIndices = [];
  tiposCuadrillas = [];
  listaCuadrillaFases = [];
  constructor(
    private _formBuilder: FormBuilder,
    private destroySubscription$: DestroySubscriptionService,
    private store: Store<GestionProduccionState>,
    private cuadrillasService: CuadrillasService,
    private progressSpinnerService: ProgressSpinnerService,
    private toastrService: ToastrService
  ) {}

  ngOnInit(): void {
    this.generarForm();
    this.store
      .pipe(
        select(selTipoObjetoSeleccionado),
        takeUntil(this.destroySubscription$)
      )
      .subscribe((tipoObjetoSeleccionado) => {
        if (tipoObjetoSeleccionado && tipoObjetoSeleccionado === 'cuadrilla') {
          this.cargarDatosCuadrilla();
        }
      });
  }

  generarForm() {
    this.form = this._formBuilder.group({
      tipoCuadrillaControl: ['', Validators.required],
      faseControl: ['', Validators.required],
      nombreCuadrillaControl: ['', Validators.required],
      antesDeControl: ['', Validators.required],
    });
  }

  agregarFaseAlista(evt) {
    evt.preventDefault();
    evt.stopPropagation();

    const currentData = JSON.parse(JSON.stringify(this.listaFases));

    const nuevaFase = {
      orden: this.form.get('antesDeControl').value,
      nombreFase: this.form.get('nombreCuadrillaControl').value,
      activo: null,
    };

    const existe = currentData.find(
      (fase) => fase.nombreFase === nuevaFase.nombreFase
    );

    if (!existe) {
      this.listaFases = null;
      currentData.forEach((fase) => {
        if (fase.orden >= nuevaFase.orden) {
          fase.orden = fase.orden + 1;
        }
      });

      currentData.splice(nuevaFase.orden - 1, 0, nuevaFase);

      this.listaFases = currentData;
      this.listaIndices = this.listaFases.map((val) => val.orden);
      this.form.reset();
    } else {
      this.toastrService.warning(
        `La cuadrilla ${existe.nombreFase} ya esta en la lista de fases.`
      );
    }
  }

  async cargarDatosCuadrilla() {
    this.progressSpinnerService.openSpinner();

    // this.tiposCuadrillas = await this.cuadrillasService.listarTiposCuadrillas();
    this.listaFases = await this.cuadrillasService.listarFases();
    this.progressSpinnerService.closeSpinner();
  }

  async cambioFase(faseId) {
    this.progressSpinnerService.openSpinner();
    this.listaCuadrillaFases = await this.cuadrillasService.listarCuadrillasPorFase(
      faseId
    );
    this.progressSpinnerService.closeSpinner();
  }
}
