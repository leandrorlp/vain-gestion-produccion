import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCuadrillasExcepcionalesComponent } from './lista-cuadrillas-excepcionales.component';

describe('ListaCuadrillasExcepcionalesComponent', () => {
  let component: ListaCuadrillasExcepcionalesComponent;
  let fixture: ComponentFixture<ListaCuadrillasExcepcionalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCuadrillasExcepcionalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCuadrillasExcepcionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
