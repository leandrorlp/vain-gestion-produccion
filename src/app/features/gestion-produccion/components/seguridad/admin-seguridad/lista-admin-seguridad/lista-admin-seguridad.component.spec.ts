import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAdminSeguridadComponent } from './lista-admin-seguridad.component';

describe('ListaAdminSeguridadComponent', () => {
  let component: ListaAdminSeguridadComponent;
  let fixture: ComponentFixture<ListaAdminSeguridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaAdminSeguridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAdminSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
