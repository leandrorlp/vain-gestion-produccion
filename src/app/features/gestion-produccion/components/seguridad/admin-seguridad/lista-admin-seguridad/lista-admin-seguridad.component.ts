import { GuardarSemanaSeguridadId } from './../../../../store/seguridad/seguridad.actions';
import { Store } from '@ngrx/store';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewChild, Input } from '@angular/core';

import {
  ProgressSpinnerService,
  SelectorProyectoMultipleComponent,
  NegocioGlobalsService,
} from 'vain-shared';

import { AdminSeguridadService } from '../../../../services/admin-seguridad/admin-seguridad.service';
import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import { EvaluarSemanaSeguridad } from '../../../../store/seguridad/seguridad.actions';

@Component({
  selector: 'app-lista-admin-seguridad',
  templateUrl: './lista-admin-seguridad.component.html',
  styleUrls: ['./lista-admin-seguridad.component.scss'],
})
export class ListaAdminSeguridadComponent implements OnInit {
  @Input() pageSizeOptions = [5, 10, 25, 100];
  @Input() pageSize = 10;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild('selectorProyectoMultiple', { static: true })
  selectorProyectoMultiple: SelectorProyectoMultipleComponent;
  form: FormGroup;
  listaSemana = [];

  length: number;
  dataSource;
  displayedColumns: string[] = [
    'proyecto',
    'semana',
    'estado',
    'evaluador',
    'acciones',
  ];
  page = 1;
  constructor(
    private progressSpinnerService: ProgressSpinnerService,
    private _formBuilder: FormBuilder,
    private negocioGlobalsService: NegocioGlobalsService,
    private adminSeguridadService: AdminSeguridadService,
    private store: Store<GestionProduccionState>
  ) {}

  ngOnInit(): void {
    this.generarForm();
    this.cargarDatos();
  }

  generarForm() {
    this.form = this._formBuilder.group({
      semanaControl: ['', ''],
    });
  }

  async cargarDatos() {
    this.progressSpinnerService.openSpinner();
    this.selectorProyectoMultiple.projectGroups = await this.negocioGlobalsService.listarCentroCostoActivos();
    this.listaSemana = await this.adminSeguridadService.listarSemanas();
    this.progressSpinnerService.closeSpinner();
    this.listarTablaSeguridad();
  }

  reiniciarFiltros() {
    this.form.reset();
    this.page = 1;
    this.listarTablaSeguridad();
  }

  getNext(evt) {
    this.page = evt.pageIndex + 1;
    this.pageSize = evt.pageSize;
    this.listarTablaSeguridad();
  }

  obtenerRequest() {
    const proyectos =
      this.selectorProyectoMultiple.projectGroupsSel.length > 0
        ? this.selectorProyectoMultiple.projectGroupsSel.map((pr) => pr['id'])
        : null;

    return {
      proyectos: proyectos,
      semanas: this.form.get('semanaControl').value
        ? this.form.get('semanaControl').value
        : null,
      page: this.page,
      pageSize: this.pageSize,
    };
  }

  async listarTablaSeguridad() {
    this.progressSpinnerService.openSpinner();
    const res = await this.adminSeguridadService.listarTablaSeguridad(
      this.obtenerRequest()
    );
    this.dataSource = new MatTableDataSource(res.results);
    this.progressSpinnerService.closeSpinner();
  }

  async evaluar(semanaSeguridadId) {
    this.progressSpinnerService.openSpinner();
    const infoEvaluacion = await this.adminSeguridadService.verDia(
      semanaSeguridadId
    );
    this.store.dispatch(
      new EvaluarSemanaSeguridad({
        data: infoEvaluacion,
        semanaSeguridadId: semanaSeguridadId,
      })
    );
  }
}
