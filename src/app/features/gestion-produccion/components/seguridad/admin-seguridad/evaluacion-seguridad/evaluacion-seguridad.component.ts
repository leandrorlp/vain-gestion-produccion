import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';

import {
  DestroySubscriptionService,
  ProgressSpinnerService,
  AlertsService,
} from 'vain-shared';

import { GestionProduccionState } from '../../../../store/gestion-produccion.state';
import {
  selEvaluacionSemanaSeguridad,
  selSemanaSeguridadId,
} from '../../../../store/seguridad/seguridad.selectors';
import { AdminSeguridadService } from '../../../../services/admin-seguridad/admin-seguridad.service';

import * as moment_ from 'moment';
const moment = moment_;

@Component({
  selector: 'app-evaluacion-seguridad',
  templateUrl: './evaluacion-seguridad.component.html',
  styleUrls: ['./evaluacion-seguridad.component.scss'],
})
export class EvaluacionSeguridadComponent implements OnInit {
  form: FormGroup;
  data: any;
  listaEvaluaciones: any;
  listaCargos: any;
  listaCuadrillas: any;
  semanaSeguridadId: number;
  listaTablaCompromisos: any;
  listaTablaHistoricoCompromisos: any;
  constructor(
    private store: Store<GestionProduccionState>,
    private destroySubscription$: DestroySubscriptionService,
    private adminSeguridadService: AdminSeguridadService,
    private progressSpinnerService: ProgressSpinnerService,
    private _formBuilder: FormBuilder,
    private alertsService: AlertsService
  ) {}

  ngOnInit(): void {
    this.generarForm();
    this.store
      .pipe(
        select(selEvaluacionSemanaSeguridad),
        takeUntil(this.destroySubscription$)
      )
      .subscribe((informacionEvaluacion) => {
        if (informacionEvaluacion) {
          this.data = informacionEvaluacion['data'];
          this.semanaSeguridadId = informacionEvaluacion['semanaSeguridadId'];
          this.cargarDatos();
        }
      });
  }

  async cargarDatos() {
    this.progressSpinnerService.openSpinner();
    this.listaEvaluaciones = await this.adminSeguridadService.listarEvaluaciones();
    this.listaCargos = await this.adminSeguridadService.listarCargos(
      this.semanaSeguridadId
    );
    this.listaCuadrillas = await this.adminSeguridadService.listarCuadrillas(
      this.semanaSeguridadId
    );
    await this.obtenerCompromisos();
    this.progressSpinnerService.closeSpinner();
    setTimeout(async () => {
      document.querySelector('#evaluacion').scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'start',
      });
    }, 250);
  }

  obtenerFecha(fecha) {
    const dateToCompare = moment(fecha);
    const today = moment(new Date());
    return dateToCompare.startOf('day').isSame(today.startOf('day'));
  }

  asignarEvaluacion(row, item?) {
    if (item) {
      row['evalSel'] = item;
      row['evaluacionId'] = item['evaluacionId'];
    } else {
      row['evalSel'] = null;
      row['evaluacionId'] = null;
    }
  }

  generarForm() {
    this.form = this._formBuilder.group({
      observacionesControl: ['', ''],
      compromisoControl: ['', Validators.required],
      fechaCompromisoControl: ['', Validators.required],
    });
  }

  async agregarCompromiso() {
    this.progressSpinnerService.openSpinner();
    const request = {
      texto: this.form.get('compromisoControl').value,
      fecha: this.form.get('fechaCompromisoControl').value,
      semanaSeguridadId: this.semanaSeguridadId,
    };
    await this.adminSeguridadService.agregarEditarCompromiso(request);
    await this.obtenerCompromisos();
    this.progressSpinnerService.closeSpinner();
  }

  async obtenerCompromisos() {
    const data = await this.adminSeguridadService.listarCompromisosEvaluacion(
      this.semanaSeguridadId
    );
    this.listaTablaCompromisos = data.actuales;
    this.listaTablaHistoricoCompromisos = data.historicos;
  }

  async eliminarCompromiso(row) {
    await this.alertsService
      .confirm(`Desea eliminar el compromiso:  ${row.compromiso} ?`, false)
      .then((res) => {
        if (typeof res === 'object') {
          this.progressSpinnerService.openSpinner();
          this.adminSeguridadService
            .eliminarCompromiso(row.compromisoId)
            .then(async () => {
              this.progressSpinnerService.closeSpinner();
              await this.obtenerCompromisos();
            })
            .catch(() => {
              this.progressSpinnerService.closeSpinner();
            });
        }
      });
  }
}
