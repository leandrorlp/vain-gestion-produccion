import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluacionSeguridadComponent } from './evaluacion-seguridad.component';

describe('EvaluacionSeguridadComponent', () => {
  let component: EvaluacionSeguridadComponent;
  let fixture: ComponentFixture<EvaluacionSeguridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluacionSeguridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluacionSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
