import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainAdminSeguridadComponent } from './main-admin-seguridad.component';

describe('MainAdminSeguridadComponent', () => {
  let component: MainAdminSeguridadComponent;
  let fixture: ComponentFixture<MainAdminSeguridadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainAdminSeguridadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainAdminSeguridadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
