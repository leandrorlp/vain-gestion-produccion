import { MainAdminSeguridadComponent } from './main-admin-seguridad/main-admin-seguridad.component';
import { ListaAdminSeguridadComponent } from './lista-admin-seguridad/lista-admin-seguridad.component';
import { EvaluacionSeguridadComponent } from './evaluacion-seguridad/evaluacion-seguridad.component';

export const CompAdminSeguridad = [
  MainAdminSeguridadComponent,
  ListaAdminSeguridadComponent,
  EvaluacionSeguridadComponent
];
