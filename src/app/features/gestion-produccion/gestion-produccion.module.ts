import {
  NgModule,
  InjectionToken,
  APP_INITIALIZER,
  ModuleWithProviders,
  CUSTOM_ELEMENTS_SCHEMA,
  LOCALE_ID,
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { Config } from './gestion-produccion.config';
import { IEnv } from 'vain-core';
import { gestionProduccionReducers } from './store/gestion-produccion.state';

// Modules
import { GestionProduccionRouting } from './routes/gestion-produccion-routing.module';
import { SharedModule } from 'vain-shared';
import { StoreModule } from '@ngrx/store';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { TableModule as PrimeNGtable } from 'primeng/table';
import { NgSelectModule } from '@ng-select/ng-select';

// Services
import { CuadrillasService } from './services/cuadrillas/cuadrillas.service';
import { LongRequestService } from './services/long-request/long-request.service';
import { ConfigService } from './services/ngx-easy-table/config.service';
import { RequestsService } from './services/requests.service';
import { TipoProyectoService } from './services/tipo-proyecto/tipo-proyecto.service';

// Components
import { MenuComponent } from './components/menu/menu.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CompAdminCuadrillas } from './components/cuadrillas/admin-cuadrillas/index';
import {
  CompMantenedorCuadrillas,
  EntryCompMantenedorCuadrillas,
} from './components/cuadrillas/mantenedor-cuadrillas/index';
import {
  CompTipoProyecto,
  EntriesTipoProyecto,
} from './components/tipo-proyecto/index';
import { CompAdminSeguridad } from './components/seguridad/admin-seguridad/index';
import { AdminSeguridadService } from './services/admin-seguridad/admin-seguridad.service';
import {ContextMenuModule} from 'primeng/contextmenu';

export const CONFIG = new InjectionToken<string>('CONFIG');
export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    MenuComponent,
    DashboardComponent,
    CompAdminCuadrillas,
    CompMantenedorCuadrillas,
    CompTipoProyecto,
    CompAdminSeguridad
  ],
  imports: [
    StoreModule.forFeature(
      'gestionProduccionFeatureModule',
      gestionProduccionReducers
    ),
    GestionProduccionRouting,
    NgxMaskModule.forRoot(options),
    SharedModule,
    ContextMenuModule,
    NgSelectModule,
    PrimeNGtable,
  ],
  providers: [
    DatePipe,
    RequestsService,
    ConfigService,
    TipoProyectoService,
    LongRequestService,
    CuadrillasService,
    AdminSeguridadService,
    { provide: LOCALE_ID, useValue: 'es-CL' },
  ],
  entryComponents: [EntryCompMantenedorCuadrillas, EntriesTipoProyecto],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class GestionProduccionModule {
  static forRoot(conf: GestionProduccionModuleConfig): ModuleWithProviders {
    return {
      ngModule: GestionProduccionModule,
      providers: [
        {
          provide: CONFIG,
          useValue: conf,
        },
        {
          provide: APP_INITIALIZER,
          useFactory: initialize,
          deps: [CONFIG],
          multi: true,
        },
      ],
    };
  }

  static forChild(conf: GestionProduccionModuleConfig): ModuleWithProviders {
    return {
      ngModule: GestionProduccionModule,
      providers: [
        {
          provide: CONFIG,
          useValue: conf,
        },
        {
          provide: APP_INITIALIZER,
          useFactory: initialize,
          deps: [CONFIG],
          multi: true,
        },
      ],
    };
  }

  static initialize(config: any) {
    Config.Configure(config);
    return function () {};
  }
}

export interface GestionProduccionModuleConfig {
  environment?: number;
  customEnv?: IEnv;
}

export function initialize(config: any) {
  Config.Configure(config);
  return function () {};
}
