import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { RequestHandlerService, Servicios } from 'vain-shared';

@Injectable()
export class CuadrillasService {
  constructor(private _requests: RequestHandlerService) {}

  listarEstandar(): Promise<any> {
    const url = 'TipoProyecto/ListarEstandar';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarDepartamentos(): Promise<any> {
    const url = 'TipoProyecto/ListarDepartamentos';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarZonaComun(): Promise<any> {
    const url = 'TipoProyecto/ListarZonaComun';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarCuadrillasAdm(req): Promise<any> {
    const url = 'Cuadrillas/ListarCuadrillasAdm';
    return this._requests.doPost(Servicios.RRHH, url, req);
  }

  listarFases(): Promise<any> {
    const url = 'Cuadrillas/ListarFases';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarCuadrillas(): Promise<any> {
    const url = 'Cuadrillas/ListarCuadrillas';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarTiposCuadrillas(): Promise<any> {
    const url = 'Cuadrillas/ListarTiposCuadrillas';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarCuadrillasPorFase(cuadrillaId): Promise<any> {
    const httpParams = new HttpParams().set('CuadrillaId', cuadrillaId);
    const url = 'Cuadrillas/ListarCuadrillasPorFase';
    return this._requests.doGet(Servicios.RRHH, url, httpParams);
  }

  listarNiveles(): Promise<any> {
    const url = 'Cuadrillas/ListarNiveles';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarEspecialidades(): Promise<any> {
    const url = 'Cuadrillas/ListarEspecialidades';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  obtenerCuadrillasBase(req): Promise<any> {
    const url = 'Cuadrillas/ObtenerCuadrillasBase';
    return this._requests.doPost(Servicios.RRHH, url, req);
  }

  obtenerCuadrillasParticulares(req): Promise<any> {
    const url = 'Cuadrillas/ObtenerCuadrillasParticulares';
    return this._requests.doPost(Servicios.RRHH, url, req);
  }

  obtenerCuadrillasExcepcionales(req): Promise<any> {
    const url = 'Cuadrillas/ObtenerCuadrillasExcepcionales';
    return this._requests.doPost(Servicios.RRHH, url, req);
  }

  estandarizarDesEstCuadrilla(cuadrillaId): Promise<any> {
    const httpParams = new HttpParams().set('CuadrillaId', cuadrillaId);
    const url = 'Cuadrillas/EstandarizarDesEstCuadrilla';
    return this._requests.doGet(Servicios.RRHH, url, httpParams);
  }

  activarDesactivarCuadrilla(cuadrillaId): Promise<any> {
    const httpParams = new HttpParams().set('CuadrillaId', cuadrillaId);
    const url = 'Cuadrillas/ActivarDesactivarCuadrilla';
    return this._requests.doGet(Servicios.RRHH, url, httpParams);
  }

  eliminarCuadrilla(cuadrillaId): any {
    const httpParams = new HttpParams().set('CuadrillaId', cuadrillaId);
    const url = 'Cuadrillas/EliminarCuadrilla';
    return this._requests.doDelete(Servicios.RRHH, url, httpParams);
  }

  activarDesactivarMatrizCuadrilla(matrizCuadrillaId): Promise<any> {
    const httpParams = new HttpParams().set('MatrizCuadrillaId', matrizCuadrillaId);
    const url = 'Cuadrillas/ActivarDesactivarMatrizCuadrilla';
    return this._requests.doGet(Servicios.RRHH, url, httpParams);
  }

  editarCantidadesTeoricas(req): Promise<any> {
    const url = 'Cuadrillas/EditarCantidadesTeoricas';
    return this._requests.doPost(Servicios.RRHH, url, req);
  }

}
