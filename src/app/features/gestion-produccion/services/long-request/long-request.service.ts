import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from 'vain-core';
import { timeout, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class LongRequestService {
  urlBase = '';
  timeOut = 240000;

  constructor(private http: HttpClient) {
    this.urlBase = Config.getApiEnvUrl();
  }

  doGet(port, url, params?) {
    const httpHeaders = new HttpHeaders().set('Accept', 'application/json');
    const responseType: any = 'json';
    const bodyRequest = {
      headers: httpHeaders,
      responseType: responseType
    };

    if (params) {
      bodyRequest['params'] = params;
    }

    return new Promise((resolve, reject) => {
      this.http
        .get(this.urlBase + port + url, bodyRequest)
        .pipe(
          timeout(this.timeOut),
          catchError(e => {
            return throwError('timeoutError');
          })
        )
        .toPromise()
        .then(res => {
          resolve(res);
        })
        .catch(err => reject(err));
    });
  }

  doPut(port, url, body) {
    const httpHeaders = new HttpHeaders().set(
      'content-type',
      'application/json'
    );

    return new Promise((resolve, reject) => {
      this.http
        .put(this.urlBase + port + url, body, {
          headers: httpHeaders
        })
        .pipe(
          timeout(this.timeOut),
          catchError(e => {
            return throwError('timeoutError');
          })
        )
        .toPromise()
        .then(res => {
          resolve(res);
        })
        .catch(err => reject(err));
    });
  }

  doPost(port, url, body) {
    const httpHeaders = new HttpHeaders().set(
      'content-type',
      'application/json'
    );

    return new Promise((resolve, reject) => {
      this.http
        .post(this.urlBase + port + url, body, {
          headers: httpHeaders
        })
        .pipe(
          timeout(this.timeOut),
          catchError(e => {
            return throwError('timeoutError');
          })
        )
        .toPromise()
        .then(res => {
          resolve(res);
        })
        .catch(err => reject(err));
    });
  }

  doDelete(port, url, params) {
    const httpHeaders = new HttpHeaders().set(
      'content-type',
      'application/json'
    );

    return this.http.delete(this.urlBase + port + url, {
      params: params,
      headers: httpHeaders,
      responseType: 'json'
    });
  }
}
