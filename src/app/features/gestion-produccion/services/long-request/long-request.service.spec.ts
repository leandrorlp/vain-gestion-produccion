/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LongRequestService } from './long-request.service';

describe('Service: LongRequest', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LongRequestService]
    });
  });

  it('should ...', inject([LongRequestService], (service: LongRequestService) => {
    expect(service).toBeTruthy();
  }));
});
