/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TipoProyectoService } from './tipo-proyecto.service';

describe('Service: TipoProyecto', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoProyectoService]
    });
  });

  it('should ...', inject([TipoProyectoService], (service: TipoProyectoService) => {
    expect(service).toBeTruthy();
  }));
});
