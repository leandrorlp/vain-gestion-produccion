import { Injectable } from '@angular/core';
import { RequestHandlerService, Servicios } from 'vain-shared';
import { LongRequestService } from '../long-request/long-request.service';

@Injectable({
  providedIn: 'root'
})
export class TipoProyectoService {

constructor(
  private _requests: RequestHandlerService,
  private _longRequest: LongRequestService
) { }

listarDepartamentos() {
  const url = 'TipoProyecto/ListarDepartamentos';
  return this._requests.doGet(Servicios.RRHH, url);
}

listarZonasComunes() {
  const url = 'TipoProyecto/ListarZonaComun';
  return this._requests.doGet(Servicios.RRHH, url);
}

listarEstandar() {
  const url = 'TipoProyecto/ListarEstandar';
  return this._requests.doGet(Servicios.RRHH, url);
}

listarTiposProyecto(req) {
  const url = 'TipoProyecto/ListarTipoProyectos';
  return this._requests.doPost(Servicios.RRHH, url, req);
}

crearEditar(req) {
  const url = 'TipoProyecto/CrearEditarTipo';
  return this._requests.doPost(Servicios.RRHH, url, req);
}

}
