import { TestBed } from '@angular/core/testing';

import { AdminSeguridadService } from './admin-seguridad.service';

describe('AdminSeguridadService', () => {
  let service: AdminSeguridadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AdminSeguridadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
