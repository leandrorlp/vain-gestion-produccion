import { Injectable } from '@angular/core';
import { RequestHandlerService, Servicios } from 'vain-shared';
import { HttpParams } from '@angular/common/http';

@Injectable()
export class AdminSeguridadService {
  constructor(private _requests: RequestHandlerService) {}

  listarSemanas(): Promise<any> {
    const url = 'Seguridad/ListarSemanas';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarTablaSeguridad(req): Promise<any> {
    const url = 'Seguridad/ListarTablaSeguridad';
    return this._requests.doPost(Servicios.RRHH, url, req);
  }

  verDia(semanaSeguridadId): Promise<any> {
    const httpParams = new HttpParams().set('SemanaSeguridadId', semanaSeguridadId);
    const url = 'Seguridad/VerDia';
    return this._requests.doGet(Servicios.RRHH, url, httpParams);
  }

  listarEvaluaciones(): Promise<any> {
    const url = 'Seguridad/ListarEvaluaciones';
    return this._requests.doGet(Servicios.RRHH, url);
  }

  listarCuadrillas(semanaSeguridadId): Promise<any> {
    const httpParams = new HttpParams().set('SemanaSeguridadId', semanaSeguridadId);
    const url = 'Seguridad/ListarCuadrillas';
    return this._requests.doGet(Servicios.RRHH, url, httpParams);
  }

  listarCargos(semanaSeguridadId): Promise<any> {
    const httpParams = new HttpParams().set('SemanaSeguridadId', semanaSeguridadId);
    const url = 'Seguridad/ListarCargos';
    return this._requests.doGet(Servicios.RRHH, url, httpParams);
  }

  agregarEditarCompromiso(req): Promise<any> {
    const url = 'Seguridad/AgregarEditarCompromiso';
    return this._requests.doPost(Servicios.RRHH, url, req);
  }

  listarCompromisosEvaluacion(semanaSeguridadId): Promise<any> {
    const httpParams = new HttpParams().set('SemanaSeguridadId', semanaSeguridadId);
    const url = 'Seguridad/ListarCompromisosEvaluacion';
    return this._requests.doGet(Servicios.RRHH, url, httpParams);
  }

  eliminarCompromiso(req): Promise<any> {
    const url = 'Seguridad/EliminarCompromiso';
    return this._requests.doPost(Servicios.RRHH, url, req);
  }

}
