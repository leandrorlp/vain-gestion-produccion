import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-private-container',
  templateUrl: './private-container.component.html',
  styleUrls: ['./private-container.component.scss']
})
export class PrivateContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
